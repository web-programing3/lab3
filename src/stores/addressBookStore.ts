import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
 interface AddressBooK{
    id:number
    name:string
    tel:string
    gender:string
  }
export const useAddressBookStore = defineStore('address_book', () => {
 
  let lastId =1
  const address = ref<AddressBooK>({
    id:0,
    name:'',
    tel:'',
    gender:'Male'
  })
  const addressList = ref<AddressBooK[]>([])
  const isAddnew =ref(false)
  function save(){
    if(address.value.id>0){
      const editedIndex = addressList.value.findIndex((item) => item.id===address.value.id)
      addressList.value[editedIndex] = address.value 
    }else{
    addressList.value.push({...address.value, id:lastId++})
    }
    isAddnew.value = false
    address.value = {
    id:0,
    name:'',
    tel:'',
    gender:'Male'
  }
  
  }
  
  function edit(id:number){
    isAddnew.value = true
    const editedIndex = addressList.value.findIndex((item) => item.id===id)
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
  }
  function remove(id:number){
    const  removeIndex = addressList.value.findIndex((item) => item.id===id)
    addressList.value.splice( removeIndex,1)
  }
  function cancel(){
    isAddnew.value = false
    address.value = {
    id:0,
    name:'',
    tel:'',
    gender:'Male'
  }
  
  }

  return {address,addressList,save,edit,isAddnew,remove,cancel}
})
